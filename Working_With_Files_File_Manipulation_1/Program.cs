﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Working_With_Files_File_Manipulation_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string folder_to_use = @"E:\ayyo\created_files";
            string folder_to_move = @"E:\ayyo\moved_files";

            //here, I will accept a file name with which a text document will be created
            Console.WriteLine("enter a file name , something like hello.txt or hello ");
            string file_name = Console.ReadLine();

            //method that will create the file requested
            create_the_file(file_name,folder_to_use);

            //show all the files in the directory
            show_all_files(folder_to_use);

            Console.WriteLine("Enter the file name to delete");
            file_name = Console.ReadLine();
            //delete the selected file
            delete_the_file(file_name,folder_to_use);

            Console.WriteLine("Enter the file name to move");
            file_name = Console.ReadLine();
            //move the selected file
            move_the_file(file_name, folder_to_move,folder_to_use);

            Console.WriteLine("That's all folks");
            Console.WriteLine("winter is coming");
            Console.ReadLine();

        }

        private static void move_the_file(string file_name, string folder_to_move,string folder_to_use)
        {
            string full_file_name = "";

            if (file_name.Contains(".txt"))
            {
                full_file_name = file_name;
            }
            else
            {
                full_file_name = file_name + ".txt";
            }


            //creating the full path for the current location of the file to be moved
            string full_path_source = Path.Combine(folder_to_use, full_file_name);
            //creating the full path for the destination location 
            string full_path_destination = Path.Combine(folder_to_move, full_file_name);

            //one file object with the current file location path
            FileInfo file_object = new FileInfo(full_path_source);

            //we must check if the file exists. in the first place
            if (File.Exists(full_path_source))
            {
                file_object.MoveTo(full_path_destination);
                Console.WriteLine("File {0} in {1} moved to {2}", full_file_name, folder_to_use,folder_to_move);

            }
            else
            {
                Console.WriteLine("File {0} in {1} does not exist, so cannot be deleted", full_file_name, folder_to_use);
            }
        }

        private static void show_all_files(string folder_to_use)
        {
            //a directory object
            DirectoryInfo directory_object = new DirectoryInfo(folder_to_use);

            //getting the list of all files
            var all_files = directory_object.GetFiles();


            foreach (FileInfo temp_file in all_files)
            {
                //display the current file name in the loop
                Console.WriteLine(temp_file.Name);
                
            }

            Console.WriteLine("Total files in {0} is {1}", folder_to_use, all_files.Length);
        }

        private static void delete_the_file(string file_name, string folder_to_use)
        {
            string full_file_name = "";

            if (file_name.Contains(".txt"))
            {
                full_file_name = file_name;
            }
            else
            {
                full_file_name = file_name + ".txt";
            }
            

            //creating the full path.
            string full_path = Path.Combine(folder_to_use, full_file_name);

            //one file object
            FileInfo file_object = new FileInfo(full_path);

            //we must check if the file exists. in the first place
            if (File.Exists(full_path))
            {
                file_object.Delete();
                Console.WriteLine("File {0} in {1} deleted", full_file_name, folder_to_use);
                
            }
            else
            {
                Console.WriteLine("File {0} in {1} does not exist, so cannot be deleted", full_file_name, folder_to_use);
            }

        }

        private static void create_the_file(string file_name, string folder_to_use)
        {
            string full_file_name = file_name+".txt";

            //creating the full path.
            string full_path = Path.Combine(folder_to_use, full_file_name);

            //one file object
            FileInfo file_object = new FileInfo(full_path);

            //checking if the file exists
            if (File.Exists(full_path))
            {
                Console.WriteLine("File {0} already exists in {1}", full_file_name, folder_to_use);
            }
            else
            {
                //creating the file
                file_object.Create();
                Console.WriteLine("File {0} created in {1}", full_file_name, folder_to_use);
            }
        }


    }
}
